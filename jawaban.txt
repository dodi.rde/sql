Soal 1
create database myshop;
use myshop;

Soal 2
create table users(
    -> id int primary key auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );
describe users;

create table categories(
    -> id int primary key auto_increment,
    -> name varchar(255)
    -> );
describe categories;

create table items(
    -> id int primary key auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int,
    -> stock int,
    -> category_id int references categories on update cascade on delete cascade
    -> );
describe items;

Soal 3
insert into users values('','John Doe','john@doe.com','john123');
insert into users values('','Jane Doe','jane@doe.com','jenita123');
select * from users;

insert into categories values('','gadget');
insert into categories values('','cloth');
insert into categories values('','men');
insert into categories values('','women');
insert into categories values('','branded');
select*from categories;

insert into items values('','Sumsang b50','hape keren dari merek sumsang',4000000,100,1);
insert into items values('','Uniklooh','baju keren dari brand ternama',500000,50,2);
insert into items values('','IMHO Watch','jam tangan anak yang jujur banget',2000000,10,1);
select*from items;

Soal 4
A
select id, name, email from users;

B
select * from items where price>1000000;
select * from items where name like '%sang%';

C
select it.name, it.description, it.price, it.stock, it.category_id,ct.name as kategori from items it, categories ct where ct.id=it.category_id;

Soal 5
update items set price=2500000 where name like '%sumsang%';
select * from items where name like '%sang%';